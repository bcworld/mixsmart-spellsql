package com.mixsmart.spellsql;


import com.mixsmart.spellsql.ICondition;
import com.mixsmart.spellsql.utils.Cnd;
import com.mixsmart.spellsql.utils.Exp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

/**
 * 测试不为空条件
 * @author lmq <br />
 * 2016年8月19日
 * @version 1.0
 * @since 1.0
 */
public class IsNotNullExpressionTest {

	@Test
	public void isNotNullTest() {
		//拼接条件语句
		ICondition cnd = Cnd.where(Exp.eq("name", "张三").and(Exp.isNotNull("age").and(Exp.eq("page", 1))));
		//获取条件语句
		String actualSqlWhere = cnd.build();
		//获取条件语句中的参数
		Map<String, Object> param = cnd.getParameters();
		String expectedSql = " where name=:name and age is not null and page=:page";
		System.out.println(actualSqlWhere);
		Assertions.assertEquals(expectedSql, actualSqlWhere);
	}
	
}
