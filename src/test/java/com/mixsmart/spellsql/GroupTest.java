package com.mixsmart.spellsql;

import com.mixsmart.spellsql.utils.Cnd;
import com.mixsmart.spellsql.utils.Exp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @autor 乌草坡 2020-03-26
 * @since 1.0
 */
public class GroupTest {


    @Test
    public void test() {
        ICondition cnd = Cnd.where(Exp.eq("flag", "corp_admin")).group("name").group("age").group(Exp.group("flag"));
        String actualCnd = cnd.build();
        String expectedCnd = " where flag=:flag group by name,age,flag";
        System.out.println(actualCnd);
        Assertions.assertEquals(expectedCnd, actualCnd);
    }

    @Test
    public void testOrder() {
        ICondition cnd = Cnd.where(Exp.eq("flag", "corp_admin")).group("name").group("age").group(Exp.group("flag")).order(Exp.desc("name"));
        String actualCnd = cnd.build();
        String expectedCnd = " where flag=:flag group by name,age,flag order by name desc";
        System.out.println(actualCnd);
        Assertions.assertEquals(expectedCnd, actualCnd);
    }
}
