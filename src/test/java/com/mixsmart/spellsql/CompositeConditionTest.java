package com.mixsmart.spellsql;

import com.mixsmart.enums.YesNoType;
import com.mixsmart.spellsql.utils.Cnd;
import com.mixsmart.spellsql.utils.Exp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @autor 乌草坡 2020-04-15
 * @since 1.0
 */
public class CompositeConditionTest {

    @Test
    public void cnd() {
        String corpId = "123";
        ICondition condition = Cnd.where(Exp.eq("corpId", corpId)
                .and(Exp.eq("state", YesNoType.YES.getIndex()))).order(Exp.asc("parentId"));
        String actualCnd = condition.build();
        String expectedCnd = " where corpId=:corpId and state=:state order by parentId asc";
        System.out.println(actualCnd);
        Assertions.assertEquals(expectedCnd, actualCnd);
    }

}
