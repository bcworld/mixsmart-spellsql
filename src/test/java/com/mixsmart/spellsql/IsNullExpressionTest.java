package com.mixsmart.spellsql;

import com.mixsmart.spellsql.utils.Cnd;
import com.mixsmart.spellsql.utils.Exp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 测试为空条件
 * @author lmq <br />
 * 2016年8月19日
 * @version 1.0
 * @since 1.0
 */
public class IsNullExpressionTest {

	@Test
	public void isNullTest() {
		ICondition cnd = Cnd.where(Exp.eq("name", "张三").and(Exp.isNull("age")));
		String actualSql = " where name=:name and age is null";
		String sqlWhere = cnd.build();
		System.out.println(sqlWhere);
		Assertions.assertEquals(sqlWhere, actualSql);
	}
	
}
