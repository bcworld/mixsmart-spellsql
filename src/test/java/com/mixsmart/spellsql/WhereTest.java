package com.mixsmart.spellsql;

import com.mixsmart.spellsql.utils.Cnd;
import com.mixsmart.spellsql.utils.Exp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class WhereTest {

    @Test
    public void multiCnd() {
       ICondition cnd = Cnd.where(Exp.eq("flag", "corp_admin").andWrap(Exp.eq("corpId", "123").or(Exp.isNull("corpId")).or(Exp.empty("corpId"))));
       String actualCnd = cnd.build();
       String expectedCnd = " where flag=:flag and (corpId=:corpId or corpId is null or corpId='')";
       System.out.println(actualCnd);
        Assertions.assertEquals(expectedCnd, actualCnd);
    }
    
}
