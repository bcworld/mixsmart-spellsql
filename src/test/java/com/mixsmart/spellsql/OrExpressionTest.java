package com.mixsmart.spellsql;

import com.mixsmart.spellsql.utils.Cnd;
import com.mixsmart.spellsql.utils.Exp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author 乌草坡
 * @since 1.0
 */
public class OrExpressionTest {

    @Test
    public void testSameField() {
        ICondition cnd = Cnd.where(Exp.eq("flag", "corp_admin").or(Exp.eq("flag", "123")));
        String actualCnd = cnd.build();
        String expectedCnd = " where flag=:flag or flag=:flag1";
        System.out.println(actualCnd);
        Assertions.assertEquals(expectedCnd, actualCnd);
    }

}
