package com.mixsmart.spellsql;

import com.mixsmart.enums.YesNoType;
import com.mixsmart.spellsql.utils.Cnd;
import com.mixsmart.spellsql.utils.Exp;
import com.mixsmart.spellsql.utils.UpdateField;
import com.mixsmart.utils.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

/**
 * @author 乌草坡 2023-10-08
 * @since 5.12.5
 */
public class UpdateSetExpressionTest {

    @Test
    public void setTest() {

        String corpId = "123";

        ICondition condition = Cnd.where(Exp.eq("corpId", corpId)
                .and(Exp.eq("state", YesNoType.YES.getIndex()))).order(Exp.asc("parentId"));

        ISpellSqlStatement statement = UpdateField.set("name", "张三").set("age", 20).where(condition);
        String actualStatement = statement.build();
        Map<String, Object> params = statement.getParameters();
        System.out.println(actualStatement);
        Assertions.assertNotNull(actualStatement);
        Assertions.assertNotNull(params);
        String paramStr = StringUtils.map2String(params);
        System.out.println(paramStr);

        String expectedStatement = " set name=:name, age=:age where corpId=:corpId and state=:state order by parentId asc";
        Assertions.assertEquals(expectedStatement, actualStatement);

        /*ICondition condition = Cnd.where(Exp.eq("corpId", corpId)
                .and(Exp.eq("state", YesNoType.YES.getIndex()))).order(Exp.asc("parentId"));
        String actualCnd = condition.build();
        String expectedCnd = " where corpId=:corpId and state=:state order by parentId asc";
        System.out.println(actualCnd);
        Assertions.assertEquals(expectedCnd, actualCnd);*/


    }

}
