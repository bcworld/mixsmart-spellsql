package com.mixsmart.spellsql;

import com.mixsmart.spellsql.utils.Cnd;
import com.mixsmart.spellsql.utils.Exp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @autor 乌草坡 2020-03-26
 * @since 1.0
 */
public class OrderTest {


    @Test
    public void test() {
        ICondition cnd = Cnd.where(Exp.eq("flag", "corp_admin")).order().order(Exp.asc("flag")).asc("name").desc("age");
        String actualCnd = cnd.build();
        String expectedCnd = " where flag=:flag order by flag asc,name asc,age desc";
        System.out.println(actualCnd);
        Assertions.assertEquals(expectedCnd, actualCnd);
    }

    @Test
    public void test2() {
        ICondition cnd = Cnd.order(Exp.asc("flag").asc("name").desc("age"));
        String actualCnd = cnd.build();
        String expectedCnd = " order by flag asc,name asc,age desc";
        System.out.println(actualCnd);
        Assertions.assertEquals(expectedCnd, actualCnd);
    }

}
