package com.mixsmart.spellsql;

import com.mixsmart.spellsql.utils.Cnd;
import com.mixsmart.spellsql.utils.Exp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * like条件测试用例
 * @author lmq
 * @version 1.0
 * @since JDK版本大于等于1.6
 * 2016年1月19日
 */
public class LikeExpressionTest {

	@Test
	public void testLike() throws Exception {
		ICondition condition = Cnd.where(Exp.like("name", "张三','"));
		String where = condition.build();
		String value = condition.getParameters().get("name").toString();
		System.out.println("=======testLike========");
		System.out.println(where);
		System.out.println(value);
		Assertions.assertEquals(where, " where name like :name");
		Assertions.assertEquals(value, "%张三,%");
	}
	
	@Test
	public void testLeftLike() throws Exception {
		ICondition condition = Cnd.where(Exp.leftLike("name", "张三','"));
		String where = condition.build();
		String value = condition.getParameters().get("name").toString();
		System.out.println("=======testLeftLike========");
		System.out.println(where);
        System.out.println(value);
		Assertions.assertEquals(where, " where name like :name");
		Assertions.assertEquals(value, "%张三,");
	}
	
	@Test
	public void testRightLike() throws Exception {
		ICondition condition = Cnd.where(Exp.rightLike("name", "','张三"));
		String where = condition.build();
		String value = condition.getParameters().get("name").toString();
		System.out.println("======testRightLike=========");
		System.out.println(where);
        System.out.println(value);
		Assertions.assertEquals(where, " where name like :name");
		Assertions.assertEquals(value, ",张三%");
	}
	
	@Test
	public void testContantRightLike() throws Exception {
        ICondition condition = Cnd.where(Exp.rightLike("name", "'张三'"));
        String where = condition.build();
        String value = condition.getParameters().get("name").toString();
        System.out.println("=======testContantRightLike========");
        System.out.println(where);
        System.out.println(value);
		Assertions.assertEquals(where, " where name like :name");
		Assertions.assertEquals(value, "张三%");
    }
	
}
