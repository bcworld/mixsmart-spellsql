package com.mixsmart.spellsql;

import com.mixsmart.spellsql.utils.Cnd;
import com.mixsmart.spellsql.utils.Exp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Set;

public class WhereWrapperTest {

    @Test
    public void wrapperCnd() {
       ICondition cnd = Cnd.where(Exp.wrapper(Exp.eq("flag", "corp_admin").or(Exp.eq("test", "2222"))).andWrap(Exp.eq("corpId", "123").or(Exp.isNull("corpId")).or(Exp.empty("corpId"))));
       String actualCnd = cnd.build();
       Map<String, Object> args = cnd.getParameters();
       String expectedCnd = " where (flag=:flag or test=:test) and (corpId=:corpId or corpId is null or corpId='')";
       System.out.println(actualCnd);
       Set<String> keys = args.keySet();
       for(String key : keys) {
           System.out.println(key + "==>"+args.get(key));
       }
        Assertions.assertEquals(expectedCnd, actualCnd);
    }
    
}
