package com.mixsmart.spellsql;

import java.util.HashMap;

/**
 * SQL语句--接口
 * @author 乌草坡 2023-10-08
 * @since 1.3.0
 */
public interface ISpellSqlStatement {

    /**
     * 组合SQL语句
     * @return 返回组合结果
     */
    <E> E build();

    /**
     * 获取参数
     * @return 返回Map参数
     */
    HashMap<String,Object> getParameters();

}
