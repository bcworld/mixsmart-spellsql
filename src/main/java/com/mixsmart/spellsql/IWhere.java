package com.mixsmart.spellsql;

/**
 * 按Where条件查询
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public interface IWhere extends ICondition {

	/**
	 * 并且--类似SQL语句里面的and
	 * @param exp 表达式 通过Exp类获取对象的表达试 <br />
	 * 如：Exp.eq("name","张三")
	 * @return 返回Where条件查询对象
	 */
	IWhere and(IWhere exp);
	
	/**
	 * 或者--类似SQL语句里面的or
	 * @param exp 表达式 通过Exp类获取对象的表达试 <br />
	 * 如：Exp.eq("name","张三")
	 * @return 返回Where条件查询对象
	 */
	IWhere or(IWhere exp);
	
	/**
	 * 并且包含，类似SQL语句里面的and加括号“and(...)”；<br />
	 * 如：and(name=?)
	 * @param where Where条件查询对象
	 * @return 返回Where条件查询对象
	 */
	IWhere andWrap(IWhere where);
	
	/**
	 * 或者包含，类似SQL语句里面的or加括号“or(...)”；<br />
	 * 如：or(name=?)
	 * @param where Where条件查询对象
	 * @return 返回Where条件查询对象
	 */
	IWhere orWrap(IWhere where);
	
}
