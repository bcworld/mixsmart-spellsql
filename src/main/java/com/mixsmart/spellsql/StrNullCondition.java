package com.mixsmart.spellsql;

import java.util.HashMap;

/**
 * 定义空条件
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public class StrNullCondition implements ICondition {

	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		return "";
	}

	@Override
	public HashMap<String, Object> getParameters() {
		return null;
	}
	
	
}
