package com.mixsmart.spellsql;

/**
 * 查询条件--接口
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public interface IQueryCondition extends ICondition {

	/**
	 * 分组查询
	 * @param group 分组对象
	 * @return 返回分组对象
	 */
    IGroup group(IGroup group);
	
    /**
     * 分组查询
     * @param fieldName 字段名称
     * @return 返回分组对象
     */
	IGroup group(String fieldName);
	
	/**
	 * 排序
	 * @param order 排序对象
	 * @return 返回排序对象
	 */
	IOrder order(IOrder order);

	/**
	 * 排序
	 * @return 返回排序对象
	 */
	IOrder order();
	
}
