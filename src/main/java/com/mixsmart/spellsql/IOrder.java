package com.mixsmart.spellsql;

/**
 * 排序
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public interface IOrder extends ICondition {

	/**
	 * 排序
	 * @param order 排序对象
	 * @return 返回排序对象
	 */
	IOrder order(IOrder order);

	/**
	 * 排序
	 * @return 返回排序对象
	 */
	IOrder order();
	
	/**
	 * 升序
	 * @param fieldName 字段名称
	 * @return 返回排序对象
	 */
	IOrder asc(String fieldName);
	
	/**
	 * 降序
	 * @param fieldName 字段名称
	 * @return 返回排序对象
	 */
	IOrder desc(String fieldName);
	
}
