package com.mixsmart.spellsql;

/**
 * 排序类型
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public enum OrderType {

	ASC("asc"),
	DESC("desc");
	
	private String value;
	
	OrderType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}
