package com.mixsmart.spellsql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 乌草坡 2023-10-08
 * @since 5.12.5
 */
public abstract class AbstractSpellSqlStatement implements ISpellSqlStatement {

    private HashMap<String,Object> params = new HashMap<>();

    private Logger logger;

    public AbstractSpellSqlStatement() {
        logger = LoggerFactory.getLogger(getClass());
    }

    /**
     * 组合参数（添加参数）
     * @param fieldName 字段名称
     * @param value 值
     * @return 返回Map对象参数
     */
    protected Map<String,Object> addParameter(String fieldName, Object value) {
        params.put(fieldName, value);
        return params;
    }

    /**
     * 获取参数的大小
     * @return
     */
    protected int getParamSize() {
        return params.size();
    }

    /**
     * 获取变量的KEY；如果key已经存在，则会在变量后面添加变量序号
     * @param fieldName 通过字段名称
     * @return 返回KEY
     */
    protected String getArgKey(String fieldName) {
        String argName = fieldName;
        if(getParameters().containsKey(argName)) {
            argName = fieldName + getParamSize();
        }
        return argName;
    }

    /**
     * 组合参数（添加参数）
     * @param leftStatement 左语句
     * @param rightStatement 右语句
     * @return 返回Map对象参数
     */
    protected Map<String,Object> addParameter(ISpellSqlStatement leftStatement, ISpellSqlStatement rightStatement) {
        if( null != leftStatement && null != leftStatement.getParameters()
                && leftStatement.getParameters().size()>0)
            params.putAll(leftStatement.getParameters());
        if(null != rightStatement && null != rightStatement.getParameters()
                && rightStatement.getParameters().size()>0)
            params.putAll(rightStatement.getParameters());
        return params;
    }

    @Override
    public HashMap<String, Object> getParameters() {
        return params;
    }

    protected Logger getLogger() {
        return logger;
    }
}
