/**
 * 
 */
package com.mixsmart.spellsql.group;

import com.mixsmart.spellsql.Condition;
import com.mixsmart.spellsql.IGroup;
import com.mixsmart.spellsql.IOrder;
import com.mixsmart.spellsql.order.NullOrder;
import com.mixsmart.spellsql.order.Order;
import com.mixsmart.spellsql.utils.Exp;

/**
 * 分组
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public abstract class CompositeGroup extends Condition implements IGroup {

	@Override
	public IOrder order(IOrder order) {
		return new Order(this, order);
	}

	@Override
	public IOrder order() {
		return new NullOrder(this);
	}

	@Override
	public IGroup group(IGroup group) {
		return new Group(this, group);
	}
	
	@Override
	public IGroup group(String fieldName) {
		return new Group(this, Exp.group(fieldName));
	}
	
}
