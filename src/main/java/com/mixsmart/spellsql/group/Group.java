package com.mixsmart.spellsql.group;

import com.mixsmart.spellsql.ICondition;
import com.mixsmart.spellsql.IGroup;

/**
 * 分组
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public class Group extends CompositeGroup {

	protected static final String GROUP_KEY = " group by ";
	
	private ICondition condition;
	
	private IGroup group;
	
	public Group(ICondition condition,IGroup group) {
		this.condition = condition;
		this.group = group;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		String key = GROUP_KEY;
		if(this.condition instanceof IGroup ) {
			key = ",";
		}
		String conditionStr = this.condition.build() + key + group.build();
		super.addParameter(condition, group);
		return conditionStr;
	}
	
}
