package com.mixsmart.spellsql.group;

import java.util.HashMap;

/**
 * 分组实现
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public class GroupLeaf extends CompositeGroup {

	private String fieldName;
	
	public GroupLeaf(String fieldName) {
		this.fieldName = fieldName;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		return fieldName;
	}

	@Override
	public HashMap<String, Object> getParameters() {
		return null;
	}
	
}
