package com.mixsmart.spellsql;

/**
 * 分组
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public interface IGroup extends ICondition {

	/**
	 * 排序
	 * @param order 
	 * @return 返回排序对象
	 */
	IOrder order(IOrder order);

	/**
	 * 排序
	 * @return 返回排序对象
	 */
	IOrder order();
	
	/**
	 * 分组
	 * @param group
	 * @return 返回分组对象
	 */
	IGroup group(IGroup group);
	
	/**
	 * 分组
	 * @param fieldName 
	 * @return 返回分组对象
	 */
	IGroup group(String fieldName);
	
}
