package com.mixsmart.spellsql;

import java.util.HashMap;

import com.mixsmart.spellsql.group.Group;
import com.mixsmart.spellsql.order.NullOrder;
import com.mixsmart.spellsql.order.Order;
import com.mixsmart.spellsql.utils.Exp;

/**
 * 组合条件实现类
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public class QueryCondition extends Condition implements IQueryCondition {

	private ICondition condition;
	
	public QueryCondition(ICondition condition) {
		this.condition = condition;
	}
	
	@Override
	public HashMap<String, Object> getParameters() {
		return condition.getParameters();
	}

	@Override
	public <E> E build() {
		return condition.build();
	}
	
	@Override
	public IGroup group(IGroup group) {
		return new Group(this, group);
	}

	@Override
	public IGroup group(String fieldName) {
		return new Group(this, Exp.group(fieldName));
	}

	@Override
	public IOrder order(IOrder order) {
		return new Order(this, order);
	}

	@Override
	public IOrder order() {
		return new NullOrder(this);
	}
}
