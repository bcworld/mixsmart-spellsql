package com.mixsmart.spellsql.where;

import com.mixsmart.spellsql.IWhere;

/**
 * OrWrap实现类
 * @author 乌草坡
 * @since 1.0
 */
public class OrWrapWhere extends OrWhere {

	private IWhere where;
	
	private IWhere exp;
	
	public OrWrapWhere(IWhere where, IWhere exp) {
		super(where, exp);
		this.where = where;
		this.exp = exp;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		super.handleSameFieldVarName();
		String conditionStr = where.build() + " or (" + exp.build() + ")";
		super.addParameter(where, exp);
		return conditionStr;
	}
	
}
