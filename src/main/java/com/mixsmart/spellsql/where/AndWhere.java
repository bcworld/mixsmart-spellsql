package com.mixsmart.spellsql.where;

import com.mixsmart.spellsql.ICondition;
import com.mixsmart.spellsql.IWhere;

/**
 * and 实现类
 * @author 乌草坡
 * @since 1.0
 */
public class AndWhere extends CompositeWhere {

	private ICondition condition;

	private IWhere exp;
	
	public AndWhere(ICondition condition, IWhere exp) {
		this.condition = condition;
		this.exp = exp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		String conditionStr = condition.build() + " and " + exp.build();
		super.addParameter(condition, exp);
		return conditionStr;
	}

}
