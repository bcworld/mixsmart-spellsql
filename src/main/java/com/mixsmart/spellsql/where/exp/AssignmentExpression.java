package com.mixsmart.spellsql.where.exp;

import com.mixsmart.utils.StringUtils;

/**
 * 直接赋值表达式
 * @author 乌草坡
 * @since 1.0
 */
public class AssignmentExpression extends AbstractExpression {

    private String fieldName;
    private String fieldVarName;
    private Object value;
    
    public AssignmentExpression(String fieldName, Object value) {
        this.fieldName = fieldName;
        this.fieldVarName = fieldName;
        this.value = value;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String build() {
        String cnd = fieldName + "=";
        if(value instanceof Integer) {
            cnd += Integer.parseInt(value.toString());
        } else if(value instanceof Double) {
            cnd += Double.parseDouble(value.toString());
        } else if(value instanceof Float) {
            cnd += Float.parseFloat(value.toString());
        } else if(value instanceof String) {
            cnd += "'" + value.toString() + "'";
        } else {
            cnd += "'" + StringUtils.handleNull(value.toString()) + "'";
        }
        return cnd;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public void setFieldVarName(String fieldVarName) {
        this.fieldVarName = fieldVarName;
    }

    @Override
    public String getFieldVarName() {
        return fieldVarName;
    }
}
