package com.mixsmart.spellsql.where.exp;

/**
 * like表达式；如：如：like "%流量%"
 * 
 * @author 乌草坡
 * @since 1.0
 * 
 */
public class LikeExpression extends AbstractLikeExpression {

    public LikeExpression(String fieldName, Object value) {
        super(fieldName, value);
    }

    @Override
    protected String getLeftLike() {
        return "%";
    }

    @Override
    protected String getRightLike() {
        return "%";
    }

    

}
