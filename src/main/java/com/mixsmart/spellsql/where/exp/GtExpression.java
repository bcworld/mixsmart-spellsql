package com.mixsmart.spellsql.where.exp;

/**
 * 大于；如:age>20
 * @author 乌草坡
 * @since 1.0
 * 
 */
public class GtExpression extends AbstractExpression {

    private String fieldName;
	private String fieldVarName;
	private Object value;
	
	public GtExpression(String fieldName,Object value) {
		this.fieldName = fieldName;
		this.fieldVarName = fieldName;
		this.value = value;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		String argName = super.getArgKey(fieldVarName);
		super.addParameter(argName, value);
		return fieldName + ">:" + argName;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Override
	public void setFieldVarName(String fieldVarName) {
		this.fieldVarName = fieldVarName;
	}

	@Override
	public String getFieldVarName() {
		return fieldVarName;
	}
}
