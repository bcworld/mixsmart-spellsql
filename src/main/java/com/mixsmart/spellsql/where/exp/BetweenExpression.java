package com.mixsmart.spellsql.where.exp;

import com.mixsmart.utils.StringUtils;

/**
 * Between 表达式；如：age between 20 and 30
 * @author 乌草坡
 * @since 1.0
 */
public class BetweenExpression extends AbstractExpression {

    private String fieldName;

    private String fieldVarName;
    
    private Object startValue;
    
    private Object endValue;
    
    public BetweenExpression(String fieldName, Object startValue, Object endValue) {
        this.fieldName = fieldName;
        this.fieldVarName = fieldName;
        this.startValue = startValue;
        this.endValue = endValue;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String build() {
        String fieldNameTmp = StringUtils.firstToUppercase(fieldVarName);
        String startName = "start" + fieldNameTmp;
        String endName = "end" + fieldNameTmp;
        super.addParameter(startName, this.startValue);
        super.addParameter(endName, this.endValue);
        return fieldName + " between :"+ startName +" and :" + endName;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public void setFieldVarName(String fieldVarName) {
        this.fieldVarName = fieldVarName;
    }

    @Override
    public String getFieldVarName() {
        return fieldVarName;
    }
}
