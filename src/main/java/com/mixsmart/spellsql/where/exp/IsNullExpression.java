package com.mixsmart.spellsql.where.exp;

/**
 * 等于null；如：name is null
 * @author 乌草坡
 * @since 1.0
 */
public class IsNullExpression extends AbstractExpression {

	private String fieldName;
	private String fieldVarName;
	
	public IsNullExpression(String fieldName) {
		this.fieldName = fieldName;
		this.fieldVarName = fieldName;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		return this.fieldName+" is null";
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Override
	public void setFieldVarName(String fieldVarName) {
		this.fieldVarName = fieldVarName;
	}

	@Override
	public String getFieldVarName() {
		return fieldVarName;
	}
}
