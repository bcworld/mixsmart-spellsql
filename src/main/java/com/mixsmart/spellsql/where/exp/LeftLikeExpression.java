package com.mixsmart.spellsql.where.exp;

/**
 * 左like；如：like "%流量"
 * @author 乌草坡
 * @since 1.0
 * 
 */
public class LeftLikeExpression extends AbstractLikeExpression {

    public LeftLikeExpression(String fieldName, Object value) {
        super(fieldName, value);
    }

    @Override
    protected String getLeftLike() {
        return "%";
    }

    @Override
    protected String getRightLike() {
        return null;
    }

    
}
