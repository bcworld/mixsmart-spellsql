package com.mixsmart.spellsql.where.exp;

/**
 * 等于空“”；如：name=''
 * @author 乌草坡
 * @since 1.0
 */
public class EmptyExpression extends AbstractExpression {

	private String fieldName;

	private String fieldVarName;
	
	public EmptyExpression(String fieldName) {
		this.fieldName = fieldName;
		this.fieldVarName = fieldName;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		return this.fieldName+"=''";
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Override
	public void setFieldVarName(String fieldVarName) {
		this.fieldVarName = fieldVarName;
	}

	@Override
	public String getFieldVarName() {
		return fieldVarName;
	}
}
