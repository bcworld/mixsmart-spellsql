package com.mixsmart.spellsql.where.exp;

import com.mixsmart.spellsql.AbstractSpellSqlStatement;
import com.mixsmart.spellsql.IExpression;
import com.mixsmart.utils.StringUtils;

/**
 * 表达式抽象类
 * @author 乌草坡
 * @since 1.0
 */
public abstract class AbstractExpression extends AbstractSpellSqlStatement implements IExpression {

    /**
     * 获取左边常量值，常量值用单引号引起来；如:"'12'value"
     * @param value 需要处理的变量
     * @return 返回常量值；如：12
     */
    protected String getLeftConstantValue(String value) {
        String cstValue = "";
        if(StringUtils.isNotEmpty(value)) {
            if(value.startsWith("'")) {
                int p = value.indexOf("'", 1);
                if(p > -1) {
                    cstValue = value.substring(1, p);
                }
            }
        }
        return cstValue;
    }

    /**
     * 获取右边常量值，常量值用单引号引起来；如:"value'12'"
     * @param value 需要处理的变量
     * @return 返回常量值；如：12
     */
    protected String getRightConstantValue(String value) {
        String cstValue = "";
        if(StringUtils.isNotEmpty(value)) {
            if(value.endsWith("'")) {
                int p = value.substring(0, value.length() - 1).lastIndexOf("'");
                if(p > -1) {
                    cstValue = value.substring(p+1, value.length()-1);
                }
            }
        }
        return cstValue;
    }

    /**
     * 去掉单引号中的值；如:"value'12'"
     * @param value 需要处理的变量
     * @return 返回处理后的值；如：value
     */
    protected String removeSingleQuoteValue(String value) {
        if(StringUtils.isNotEmpty(value)) {
            value = value.replaceAll("'.*?'", "");
        }
        return value;
    }

}
