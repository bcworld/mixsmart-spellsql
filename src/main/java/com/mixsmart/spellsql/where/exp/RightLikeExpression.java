package com.mixsmart.spellsql.where.exp;

/**
 * 右like 如：like "流量%"
 * @author 乌草坡
 * @since 1.0
 */
public class RightLikeExpression extends AbstractLikeExpression {


    public RightLikeExpression(String fieldName, Object value) {
        super(fieldName, value);
    }

    @Override
    protected String getLeftLike() {
        return null;
    }

    @Override
    protected String getRightLike() {
        return "%";
    }
    
}
