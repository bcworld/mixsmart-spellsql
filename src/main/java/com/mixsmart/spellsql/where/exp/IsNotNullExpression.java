package com.mixsmart.spellsql.where.exp;

/**
 * 不等于null；如：name is not null
 * @author 乌草坡
 * @since 1.0
 */
public class IsNotNullExpression extends AbstractExpression {

	private String fieldName;
	private String fieldVarName;
	
	public IsNotNullExpression(String fieldName) {
		this.fieldName = fieldName;
		this.fieldVarName = fieldName;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		return this.fieldName + " is not null";
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Override
	public void setFieldVarName(String fieldVarName) {
		this.fieldVarName = fieldVarName;
	}

	@Override
	public String getFieldVarName() {
		return fieldVarName;
	}
}
