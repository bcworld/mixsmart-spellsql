package com.mixsmart.spellsql.where;

import com.mixsmart.spellsql.IExpression;
import com.mixsmart.spellsql.ISpellSqlStatement;

/**
 * @author 乌草坡 2023-10-08
 * @since 5.12.5
 */
public class ConditionContext extends CompositeWhere implements IExpression {

    private ISpellSqlStatement statement;

    private IExpression exp;

    public ConditionContext(ISpellSqlStatement statement) {
        this.statement = statement;
        if(statement instanceof IExpression) {
            exp = (IExpression) statement;
        }
    }

    @Override
    public String build() {
        String conditionStr = statement.build();
        super.addParameter(statement, null);
        return conditionStr;
    }

    @Override
    public String getFieldName() {
        if(null != exp) {
            return exp.getFieldName();
        } else {
            return null;
        }
    }

    @Override
    public void setFieldVarName(String fieldVarName) {
        if(null != exp) {
            exp.setFieldVarName(fieldVarName);
        }
    }

    @Override
    public String getFieldVarName() {
        if(null != exp) {
            return exp.getFieldVarName();
        } else {
            return null;
        }
    }
}
