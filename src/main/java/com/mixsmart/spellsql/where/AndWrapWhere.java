package com.mixsmart.spellsql.where;

import com.mixsmart.spellsql.IExpression;
import com.mixsmart.spellsql.IWhere;

/**
 * AndWrap实现类
 * @author 乌草坡
 * @since 1.0
 */
public class AndWrapWhere extends AndWhere {

	private IWhere where;
	
	private IWhere exp;
	
	public AndWrapWhere(IWhere where, IWhere exp) {
		super(where, exp);
		this.where = where;
		this.exp = exp;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String build() {	
		String conditionStr = where.build() + " and (" + exp.build() + ")";
		super.addParameter(where, exp);
		return conditionStr;
	}

}
