package com.mixsmart.spellsql.where;

import com.mixsmart.spellsql.ISpellSqlStatement;

/**
 * Where条件语句
 * @author 乌草坡
 * @since 1.0
 */
public class WhereContext extends CompositeWhere {

	private ISpellSqlStatement statement;

    private ISpellSqlStatement exp;
	
	public WhereContext(ISpellSqlStatement exp) {
		this.exp = exp;
	}

	public WhereContext(ISpellSqlStatement statement, ISpellSqlStatement exp) {
		this.statement = statement;
		this.exp = exp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		String conditionStr = "";
		if(null != statement) {
			conditionStr = statement.build() + " where " + exp.build();
			super.addParameter(statement, exp);
		} else {
			conditionStr = " where " + exp.build();
			super.addParameter(exp, null);
		}
		return conditionStr;
	}
}
