package com.mixsmart.spellsql.where;

import com.mixsmart.spellsql.Condition;
import com.mixsmart.spellsql.IExpression;
import com.mixsmart.spellsql.IWhere;

/**
 * 组合条件；如and或or等
 * @author 乌草坡
 * @since 1.0
 */
public abstract class CompositeWhere extends Condition implements IWhere {
	
	@Override
	public IWhere and(IWhere exp) {
		return new AndWhere(this, exp);
	}

	@Override
	public IWhere or(IWhere exp) {
		return new OrWhere(this, exp);
	}

	@Override
	public IWhere andWrap(IWhere exp) {
		return new AndWrapWhere(this, exp);
	}

	@Override
	public IWhere orWrap(IWhere exp) {
		return new OrWrapWhere(this, exp);
	}


}
