package com.mixsmart.spellsql.where;

import com.mixsmart.spellsql.IWhere;

public class WrapperWhere extends CompositeWhere {
    
    private IWhere exp;
    
    public WrapperWhere(IWhere exp) {
        this.exp = exp;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public String build() {
        String conditionStr = "(" + exp.build() + ")";
        super.addParameter(exp, null);
        return conditionStr;
    }
    
}
