package com.mixsmart.spellsql.order;

import java.util.HashMap;

import com.mixsmart.spellsql.OrderType;

/**
 * 升序排序
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public class AscOrder extends CompositeOrder {
	
	private String fieldName;
	
	public AscOrder(final String fieldName) {
		this.fieldName = fieldName;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		return this.fieldName+" "+OrderType.ASC.getValue();
	}
	
	@Override
	public HashMap<String, Object> getParameters() {
		return null;
	}
}
