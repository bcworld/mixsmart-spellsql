package com.mixsmart.spellsql.order;

import java.util.HashMap;

import com.mixsmart.spellsql.OrderType;

/**
 * 降序排序
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public class DescOrder extends CompositeOrder {
	
	private String fieldName;
	
	public DescOrder(final String fieldName) {
		this.fieldName = fieldName;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		return this.fieldName+" " + OrderType.DESC.getValue();
	}

	@Override
	public HashMap<String, Object> getParameters() {
		return null;
	}
	
}
