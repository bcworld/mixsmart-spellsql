package com.mixsmart.spellsql.order;

import com.mixsmart.spellsql.ICondition;
import com.mixsmart.spellsql.IOrder;

/**
 * 排序
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public class Order extends CompositeOrder {

	private ICondition condition;
	
	private IOrder order;

	public Order(ICondition condition) {
		this.condition = condition;
	}

	public Order(ICondition condition, IOrder order) {
		this.condition = condition;
		this.order = order;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String build() {
		String key = ORDER_KEY;
		StringBuilder conditionBuilder = new StringBuilder();
		String cndStr = this.condition.build();
		conditionBuilder.append(cndStr);
		if(this.condition instanceof  IOrder) {
			if(this.condition instanceof NullOrder) {
				key = "";
			} else {
				key = ",";
			}
		}
		conditionBuilder.append(key);
		if(null != order) {
			cndStr = order.build();
			conditionBuilder.append(cndStr);
		}
		super.addParameter(condition, order);
		return conditionBuilder.toString();
	}
	
}
