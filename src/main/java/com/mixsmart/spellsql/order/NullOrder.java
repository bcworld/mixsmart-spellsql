package com.mixsmart.spellsql.order;

import com.mixsmart.spellsql.ICondition;
import com.mixsmart.spellsql.IOrder;

/**
 * 空的排序对象
 * @autor 乌草坡
 * @since 1.0
 */
public class NullOrder extends CompositeOrder {

    private ICondition condition;

    public NullOrder(ICondition condition) {
        this.condition = condition;
    }

    @Override
    public String build() {
        String key = ORDER_KEY;
        StringBuilder conditionBuilder = new StringBuilder();
        String cndStr = this.condition.build();
        conditionBuilder.append(cndStr);
        if(this.condition instanceof IOrder) {
            key = "";
        }
        conditionBuilder.append(key);
        super.addParameter(condition, null);
        return conditionBuilder.toString();
    }
}
