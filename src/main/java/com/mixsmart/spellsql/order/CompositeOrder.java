/**
 * 
 */
package com.mixsmart.spellsql.order;

import com.mixsmart.spellsql.Condition;
import com.mixsmart.spellsql.IOrder;

/**
 * 排序
 * @author lmq
 * @version 1.0
 * @since 1.0
 */
public abstract class CompositeOrder extends Condition implements IOrder {

	protected final String ORDER_KEY = " order by ";

	@Override
	public IOrder order(IOrder order) {
		return new Order(this, order);
	}

	@Override
	public IOrder order() {
		return new NullOrder(this);
	}

	@Override
	public IOrder asc(String fieldName) {
		return new Order(this, new AscOrder(fieldName));
	}

	@Override
	public IOrder desc(String fieldName) {
		return new Order(this, new DescOrder(fieldName));
	}
}
