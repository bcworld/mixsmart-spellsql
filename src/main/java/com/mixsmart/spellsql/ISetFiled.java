package com.mixsmart.spellsql;

/**
 * 设置字段值；用于更新字段
 * @author 乌草坡 2023-10-08
 * @since 1.3.0
 */
public interface ISetFiled extends ISpellSqlStatement {

    /**
     * 设置字段
     * @param fileName 字段名称
     * @param value 字段值
     * @return 返回设置字段对象实例
     */
    ISetFiled set(String fileName, Object value);

    /**
     * where条件查询
     * @param condition 查询条件的表达式
     * @return 返回where条件对象实例
     */
    ISpellSqlStatement where(ICondition condition);
}
