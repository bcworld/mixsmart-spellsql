package com.mixsmart.spellsql.utils;

import com.mixsmart.spellsql.*;
import com.mixsmart.spellsql.group.Group;
import com.mixsmart.spellsql.order.Order;
import com.mixsmart.spellsql.set.SetFieldContext;
import com.mixsmart.spellsql.set.SetFieldExpression;
import com.mixsmart.spellsql.where.WhereContext;

/**
 * 组合条件
 * @author 乌草坡
 * @since 1.0
 */
public class Cnd {

	/**
	 * where条件查询
	 * @param exp where条件对象
	 * @return 返回查询条件对象实例
	 */
	public static IQueryCondition where(IWhere exp) {
		return new QueryCondition(new WhereContext(exp));
	}
	
	/**
	 * group分组查询
	 * @param group 分组对象
	 * @return 返回分组对象实例
	 */
	public static IGroup group(IGroup group) {
		return new Group(new StrNullCondition(), group);
	}
	
	/**
	 * 排序
	 * @param order 排序对象
	 * @return 返回排序对象的实例
	 */
	public static IOrder order(IOrder order) {
		return new Order(new StrNullCondition(), order);
	}
	
}
