package com.mixsmart.spellsql.utils;

import com.mixsmart.spellsql.*;
import com.mixsmart.spellsql.group.GroupLeaf;
import com.mixsmart.spellsql.order.AscOrder;
import com.mixsmart.spellsql.order.DescOrder;
import com.mixsmart.spellsql.set.SetFieldContext;
import com.mixsmart.spellsql.set.SetFieldExpression;
import com.mixsmart.spellsql.where.ConditionContext;
import com.mixsmart.spellsql.where.WhereContext;
import com.mixsmart.spellsql.where.WrapperWhere;
import com.mixsmart.spellsql.where.exp.*;

/**
 * 表达式
 * @author 乌草坡
 * @since 1.0
 */
public class Exp {
	
	/**
	 * 排序--升序
	 * @param fieldName 字段名称
	 * @return 返回排序对象
	 */
	public static IOrder asc(String fieldName) {
		return new AscOrder(fieldName);
	}
	
	/**
	 * 排序--降序
	 * @param fieldName 字段名称
	 * @return 返回排序对象
	 */
	public static IOrder desc(String fieldName) {
		return new DescOrder(fieldName);
	}
	
	/**
	 * 分组
	 * @param fieldName 字段名称
	 * @return 返回分组对象
	 */
	public static IGroup group(String fieldName) {
		return new GroupLeaf(fieldName);
	}

	/**
	 * 等于
	 * @param fieldName　字段名称
	 * @param value　值
	 * @return 返回表达式对象
	 */
	public static IWhere eq(String fieldName, Object value) {
		return new ConditionContext(new EqExpression(fieldName, value));
	} 
	
	/**
	 * 包含（in）
	 * @param fieldName　字段名称
	 * @param value　值
	 * @return 返回表达式对象
	 */
	public static IWhere in(String fieldName, Object value) {
		return new ConditionContext(new InExpression(fieldName, value));
	} 
	
	/**
	 * 不包含（not in）
	 * @param fieldName　字段名称
	 * @param value　值
	 * @return 返回表达式对象
	 */
	public static IWhere notIn(String fieldName, Object value) {
		return new ConditionContext(new NotInExpression(fieldName, value));
	}
	
	/**
	 * 大于
	 * @param fieldName　字段名称
	 * @param value　值
	 * @return 返回表达式对象
	 */
	public static IWhere gt(String fieldName, Object value) {
		return new ConditionContext(new GtExpression(fieldName, value));
	}
	
	/**
	 * 大于等于
	 * @param fieldName　字段名称
	 * @param value　值
	 * @return 返回表达式对象
	 */
	public static IWhere ge(String fieldName, Object value) {
		return new ConditionContext(new GeExpression(fieldName, value));
	}
	
	/**
	 * 小于
	 * @param fieldName　字段名称
	 * @param value　值
	 * @return 返回表达式对象
	 */
	public static IWhere lt(String fieldName, Object value) {
		return new ConditionContext(new LtExpression(fieldName, value));
	}
	
	/**
	 * 小于等于
	 * @param fieldName　字段名称
	 * @param value　值
	 * @return 返回表达式对象
	 */
	public static IWhere le(String fieldName, Object value) {
		return new ConditionContext(new LeExpression(fieldName, value));
	}
	
	/**
	 * like (如：like "%流量%")
	 * @param fieldName 字段名称
	 * @param value 值
	 * @return 返回表达式对象
	 */
	public static IWhere like(String fieldName, Object value) {
		return new ConditionContext(new LikeExpression(fieldName, value));
	}
	
	/**
	 * left like (如：like "%流量")
	 * @param fieldName 字段名称
	 * @param value 值
	 * @return 返回表达式对象
	 */
	public static IWhere leftLike(String fieldName, Object value) {
		return new ConditionContext(new LeftLikeExpression(fieldName, value));
	}
	
	/**
	 * right like (如：like "流量%")
	 * @param fieldName 字段名称
	 * @param value 值
	 * @return 返回表达式对象
	 */
	public static IWhere rightLike(String fieldName, Object value) {
		return new ConditionContext(new RightLikeExpression(fieldName, value));
	}
	
	/**
	 * 为空 (如：is null)
	 * @param fieldName 字段名称
	 * @return 返回表达式对象
	 */
	public static IWhere isNull(String fieldName) {
		return new ConditionContext(new IsNullExpression(fieldName));
	}
	
	/**
     * 为空 (如：name='')
     * @param fieldName 字段名称
     * @return 返回表达式对象
     */
    public static IWhere empty(String fieldName) {
        return new ConditionContext(new EmptyExpression(fieldName));
    }
	
	/**
	 * 不为空 (如：is not null)
	 * @param fieldName 字段名称
	 * @return 返回表达式对象
	 */
	public static IWhere isNotNull(String fieldName) {
		return new ConditionContext(new IsNotNullExpression(fieldName));
	}
	
	/**
     * 直接赋值（如：name='张三'）
     * @param fieldName 字段名称
     * @param value 值
     * @return 返回表达式对象
     */
    public static IWhere assign(String fieldName, Object value) {
        return new ConditionContext(new AssignmentExpression(fieldName, value));
    }
    
    /**
     * between表达式（如：age between 20 and 30）
     * @param fieldName 字段名称
     * @param startValue 开始范围值
     * @param endValue 结束范围值
     * @return 返回表达式对象
     */
    public static IWhere between(String fieldName, Object startValue, Object endValue) {
        return new ConditionContext(new BetweenExpression(fieldName, startValue, endValue));
    }
    
    /**
     * 条件表达式用括号括起来；如：(t.name=:name or t.age>:age)
     * @param exp 表达式
     * @return 返回表达式
     */
    public static IWhere wrapper(IWhere exp) {
        return new WrapperWhere(exp);
    }

	/**
	 * 不等于（<>）
	 * @param fieldName　字段名称
	 * @param value　值
	 * @return 返回表达式对象
	 */
	public static IWhere not(String fieldName, Object value) {
		return new ConditionContext(new NotExpression(fieldName, value));
	}

	/**
	 * 设置字段表达式
	 * @param fieldName 字段名称
	 * @param value 值
	 * @return 返回字段表达式对象
	 */
	public static ISetFiled set(String fieldName, Object value) {
		return new SetFieldContext(new SetFieldExpression(fieldName, value));
	}
}
