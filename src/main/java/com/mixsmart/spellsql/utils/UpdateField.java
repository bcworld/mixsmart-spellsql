package com.mixsmart.spellsql.utils;

import com.mixsmart.spellsql.ISetFiled;
import com.mixsmart.spellsql.set.SetFieldContext;
import com.mixsmart.spellsql.set.SetFieldExpression;

/**
 * @author 乌草坡 2023-10-08
 * @since 1.3.0
 */
public class UpdateField {

    /**
     * 设置更新字段
     * @param exp 表达式
     * @return 返回字段设置对象实例
     */
    public static ISetFiled set(String fieldName, Object value) {
        return new SetFieldContext(new SetFieldExpression(fieldName, value));
    }
}
