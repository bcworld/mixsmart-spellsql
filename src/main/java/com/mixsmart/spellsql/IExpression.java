package com.mixsmart.spellsql;

/**
 * 表达式--接口
 * @author 乌草坡
 * @since 1.0
 */
public interface IExpression extends ISpellSqlStatement {

    /**
     * 获取字段名称
     * @return 返回字段名称
     */
    String getFieldName();

    /**
     * 设置字段变量名称
     * @param fieldVarName 字段对应的变量名称
     */
    void setFieldVarName(String fieldVarName);

    /**
     * 获取字段变量名称
     * @return 返回字段变量名称
     */
    String getFieldVarName();
	
}
