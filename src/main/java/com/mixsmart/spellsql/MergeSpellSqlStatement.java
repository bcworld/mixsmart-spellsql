package com.mixsmart.spellsql;

/**
 * 合并语句
 * @author 乌草坡 2023-10-08
 * @since 1.3.0
 */
public class MergeSpellSqlStatement extends AbstractSpellSqlStatement {

    private ISpellSqlStatement leftStatement;

    private ISpellSqlStatement rightStatement;

    public MergeSpellSqlStatement(ISpellSqlStatement leftStatement, ISpellSqlStatement rightStatement) {
        this.leftStatement = leftStatement;
        this.rightStatement = rightStatement;
    }

    @Override
    public String build() {
        String conditionStr = leftStatement.build() + "" + rightStatement.build();
        super.addParameter(leftStatement, rightStatement);
        return conditionStr;
    }
}
