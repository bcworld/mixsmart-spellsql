package com.mixsmart.spellsql.set;

import com.mixsmart.spellsql.*;

/**
 * 组合set字段
 * @author 乌草坡
 * @since 1.3.0
 */
public class SetFieldComposite extends AbstractSpellSqlStatement implements ISetFiled {

    private ISpellSqlStatement statement;

    private IExpression exp;

    public SetFieldComposite(ISpellSqlStatement statement, IExpression exp) {
        this.statement = statement;
        this.exp = exp;
    }

    @Override
    public String build() {
        String conditionStr = statement.build() + ", " + exp.build();
        super.addParameter(statement, exp);
        return conditionStr;
    }

    @Override
    public ISetFiled set(String fieldName, Object value) {
        return new SetFieldComposite(this, new SetFieldExpression(fieldName, value));
    }

    @Override
    public ISpellSqlStatement where(ICondition condition) {
        return new MergeSpellSqlStatement(this, condition);
    }
}
