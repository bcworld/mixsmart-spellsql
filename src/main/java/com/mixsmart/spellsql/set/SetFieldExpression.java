package com.mixsmart.spellsql.set;

import com.mixsmart.spellsql.where.exp.AbstractExpression;

/**
 * @author 乌草坡 2023-10-08
 * @since 5.12.5
 */
public class SetFieldExpression extends AbstractExpression {

    private String fieldName;

    private String fieldVarName;

    private Object value;

    public SetFieldExpression(String fieldName, Object value) {
        this.fieldName = fieldName;
        this.fieldVarName = fieldName;
        this.value = value;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public void setFieldVarName(String fieldVarName) {
        this.fieldVarName = fieldVarName;
    }

    @Override
    public String getFieldVarName() {
        return fieldVarName;
    }

    @Override
    public String build() {
        String argName = super.getArgKey(fieldVarName);
        super.addParameter(argName, value);
        return fieldName + "=:" + argName;
    }
}
