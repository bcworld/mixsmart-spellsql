package com.mixsmart.spellsql.set;

import com.mixsmart.spellsql.*;

/**
 * @author 乌草坡 2023-10-08
 * @since 5.12.5
 */
public class SetFieldContext extends AbstractSpellSqlStatement implements ISetFiled {

    private ISpellSqlStatement statement;

    public SetFieldContext(ISetFiled setFiled) {
        this.statement = setFiled;
    }

    public SetFieldContext(IExpression exp) {
        this.statement = exp;
    }

    @Override
    public String build() {
        String conditionStr = " set " + statement.build();
        super.addParameter(statement, null);
        return conditionStr;
    }

    @Override
    public ISetFiled set(String fieldName, Object value) {
        return new SetFieldComposite(this, new SetFieldExpression(fieldName, value));
    }

    @Override
    public ISpellSqlStatement where(ICondition condition) {
        return new MergeSpellSqlStatement(this, condition);
    }
}
